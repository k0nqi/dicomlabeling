from unittest import TestCase, mock

import numpy as np
from pydicom import DataElement

from DicomDataHandler import DicomDataHandler
from typing import Dict


class TestDicomDataHandler(TestCase):

    class MockDataset(Dict):

        def __init__(self, time, patient, dim2=True, workflow_phase=0):
            super().__init__()
            if dim2:
                self.pixel_array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            else:
                self.pixel_array = np.array([[[1, 2, 3], [4, 5, 6], [7, 8, 9]], [[1, 2, 3], [4, 5, 6], [7, 8, 9]]])
            self['SeriesTime'] = DataElement(tag=0x00080031, VR='TM', value=time)
            self['PatientName'] = DataElement(tag=0x00100010, VR='PN', value=patient)
            if workflow_phase:
                self[0x00331001] = DataElement(tag=0x00331001, VR='SS', value=workflow_phase)

    @mock.patch('DicomDataHandler.os.walk')
    @mock.patch('DicomDataHandler.dcmread')
    def test_load(self, mock_dcmread, mock_walk):
        mock_walk.return_value = zip(['path1', 'path2'], [['subdir1', 'subdir2'], ['subdir3', 'subdir4']],
                                     [['file1.dcm', 'file2.dcm'], ['file3.dcm', 'file4.dcm', 'file5']])
        mock_dcmread.side_effect = [self.MockDataset('101010', 'name1', workflow_phase=13), self.MockDataset('100909', 'name2'),
                                    self.MockDataset('101111', 'name3'), self.MockDataset('101212', 'name4', dim2=False)
                                    ]

        ddh = DicomDataHandler()
        ddh.load('\\not\\a\\directory')
        mock_walk.assert_called_with('\\not\\a\\directory')
        self.assertEqual(4, mock_dcmread.call_count)
        mock_dcmread.assert_has_calls([
            mock.call('path1\\file1.dcm'),
            mock.call('path1\\file2.dcm'),
            mock.call('path2\\file3.dcm'),
            mock.call('path2\\file4.dcm')
        ])
        self.assertEqual([1, 1, 1, 2], ddh.frames)
        self.assertEqual([{'time': '10:09:09'}, {'time': '10:10:10'}, {'time': '10:11:11'}, {'time': '10:12:12'}],
                         ddh.dicom_data)
        self.assertTrue(ddh.data_loaded)
        self.assertEqual('\\not\\a\\directory', ddh.data_directory)
        self.assertEqual(['path1\\file2.dcm', 'path1\\file1.dcm', 'path2\\file3.dcm', 'path2\\file4.dcm'], ddh._file_paths)
        self.assertEqual([0, 13, 0, 0], ddh.workflow_phase)
