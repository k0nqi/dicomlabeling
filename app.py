import logging

from flask import Flask, render_template, request, redirect, url_for
from DicomDataHandler import DicomDataHandler

app = Flask(__name__)
log = logging.getLogger('werkzeug')
log.disabled = True

ddh = DicomDataHandler()


@app.route('/')
def main():
    return redirect(url_for('editing'))


@app.route('/editing')
def editing():
    return render_template('main.html', series=range(len(ddh.dataset)), frames=ddh.frames, dicom_data=ddh.dicom_data,
                           data_loaded=ddh.data_loaded, data_directory=ddh.data_directory,
                           workflow_phase=ddh.workflow_phase)


@app.route('/load', methods=['POST'])
def load_data():

    data_directory = request.form['data_directory']
    print(data_directory)
    ddh.load(data_directory)

    return redirect(url_for('editing'))


@app.route('/save', methods=['POST'])
def save_data():
    print('save data')

    workflow_phase = [int(i) for i in request.form.getlist('workflow_phase')]

    ddh.save(workflow_phase)

    return redirect(url_for('editing'))


@app.route('/img/<img_id>/<frame_id>')
def image(img_id, frame_id):

    return ddh.image(img_id, frame_id)


if __name__ == '__main__':
    app.run()
