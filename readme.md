# Readme

This tool allows you to add a custom label as dicom entry (0033, 1001) to each dicom file in a directory. The tool shows all images / image series in chronological order.

This tool uses flask to display the graphical user interface. Don't use this app as an actual server application on the network.

To run the app on the flask development server, use 

    flask run

from the project directory.

You can use the app in your browser at http://localhost:5000 .

1. Enter the full path to a directory. It may contain dicom files or other directories.
2. Press 'load data'.
3. Enter a label for each file. Must be an Integer and can't be empty.
4. Press 'save data'. (at the bottom of the page)

The data is saved into the same directory and overwrites the original file, so make sure to have a backup.
