import base64
import gc
import os
from io import BytesIO

from matplotlib.figure import Figure
from pydicom import dcmread


class DicomDataHandler:

    def __init__(self):
        self._dataset = []
        self._frames = []
        self._dicom_data = []
        self._data_loaded = False
        self._data_directory = ""
        self._file_paths = []
        self._workflow_phase = []

    @property
    def dataset(self):
        return self._dataset

    @property
    def frames(self):
        return self._frames

    @property
    def dicom_data(self):
        return self._dicom_data

    @property
    def data_loaded(self):
        return self._data_loaded

    @property
    def data_directory(self):
        return self._data_directory

    @property
    def workflow_phase(self):
        return self._workflow_phase

    def load(self, data_directory):

        self.__init__()
        self._data_directory = data_directory

        try:
            for path, subdirs, files in os.walk(data_directory):
                for name in files:
                    try:
                        if not name.endswith('.dcm'):
                            break
                        exclude = ['DYNACT', 'MPR']
                        if any([x in path for x in exclude]):
                            break
                        path_complete = os.path.join(path, name)
                        print(path)
                        self._file_paths.append(path_complete)
                        data = dcmread(path_complete)
                        self._dataset.append(data)
                        dimensions = data.pixel_array.ndim
                        if dimensions == 3:
                            self._frames.append(data.pixel_array.shape[0])
                        elif dimensions == 2:
                            self._frames.append(1)
                        else:
                            print('Invalid pixel data dimension!')

                        patientname = data['PatientName'].value
                        date = data['StudyDate'].value
                        time = data['SeriesTime'].value
                        time_str = time[0:2] + ':' + time[2:4] + ':' + time[4:6]
                        self._dicom_data.append({'patientname': patientname, 'time': time_str, 'date': date,
                                                 'file': path_complete})

                        if 0x00331001 in data:
                            self._workflow_phase.append(data[0x00331001].value)
                        else:
                            self._workflow_phase.append(0)

                    except Exception as e:
                        print(e)

                        # Clean up, if some data was already added
                        data_length = len(self._workflow_phase)
                        if len(self._file_paths) > data_length:
                            self._file_paths.pop()
                        if len(self._dataset) > data_length:
                            self._dataset.pop()
                        if len(self._frames) > data_length:
                            self._frames.pop()
                        if len(self._dicom_data) > data_length:
                            self._dicom_data.pop()

                if len(self._dataset) == len(self._frames) == len(self.workflow_phase) == len(self._dicom_data):
                    self._data_loaded = True
                else:
                    print('DICOM Loading Error')

            # TODO: Write test (maybe)
            # TODO: use one datastructure
            self._dicom_data, self._dataset, self._frames, self._file_paths, self._workflow_phase = zip(*sorted(zip(
                self._dicom_data, self._dataset, self._frames, self._file_paths, self._workflow_phase),
                key=lambda x: x[0]['time']))
            self._dicom_data = list(self._dicom_data)
            self._dataset = list(self._dataset)
            self._frames = list(self._frames)
            self._file_paths = list(self._file_paths)
            self._workflow_phase = list(self._workflow_phase)

            print('data loaded')

        except Exception as e:
            print(e)

    def save(self, workflow_phase):
        self._workflow_phase = workflow_phase

        for idx, data in enumerate(self._dataset):
            try:
                block = data.private_block(0x0033, 'FAU')
            except KeyError:
                block = data.private_block(0x0033, 'FAU', create=True)

            # Add label
            try:
                block[0x01].value = workflow_phase[idx]
            except Exception as e:
                print(e)
                block.add_new(0x01, "SS", workflow_phase[idx])

            data.save_as(self._file_paths[idx])

        print('data saved: ' + self._data_directory)

    def image(self, img_id, frame_id):
        fig = Figure()
        axs = fig.subplots()
        axs.axis('off')
        if self._frames[int(img_id)] > 1:
            axs.imshow(self._dataset[int(img_id)].pixel_array[int(frame_id)], cmap='gray')
            # print('show image: ' + str(img_id) + str(frame_id))
        else:
            axs.imshow(self._dataset[int(img_id)].pixel_array, cmap='gray')
            # print('show image: ' + str(img_id))

        img = BytesIO()
        # TODO: make image size an option
        fig.set_size_inches(10.0, 10.0)
        fig.savefig(img, format='png', bbox_inches='tight', pad_inches=0.0)
        return base64.b64encode(img.getvalue())
